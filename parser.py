from abc import ABC, abstractmethod
from copy import deepcopy

class TuringParser:

    rules = list()
    states = list()

    @classmethod
    def add_rule(cls, rule):
        """
        Takes a string in a format:
         'state_name | cell_value ----> new_state_name | motion_direction | new_cell_value_name',
         ignoring space characters and number of '-' chars.
        Creates a tuple (state_name, cell_value_name, new_state_name, motion_direction, new_cell_value_name),
        adds it to 'rules' list, fills alphabets.
        """
        preproc_rule = rule.replace('-', '')
        preproc_rule = preproc_rule.replace(">", '|')
        preproc_rule = preproc_rule.replace(' ', '')
        preproc_rule = preproc_rule.replace("Em", '£')

        new_rule = tuple(preproc_rule.split('|'))
        cls.rules.append(new_rule)

    @classmethod
    def parse_table(cls):
        print("Please type the state transition table:")
        print("\t - the first row is for the cell values distinguished by spaces;")
        print("\t - the next rows have to begin with the name of some state;")
        print("\t - next notes are to be in format 'state_name, motion, new_value'.")
        print("If the table is completed, input the string '|||'")
        s = input("\t")
        states = dict()

        value_list = s.split(" ")
        for value in value_list:
            if not value:
                value_list.remove(value)

        n = len(value_list)
        
        s = input()

        while s != "|||":
            state, string = s.split(" ", 1)
            if state not in states.keys():
                states[state] = list()
                while string != "":
                    rule = list()
                    for i in range(2):
                        item, string = string.split(',', 1)
                        rule.append(item.replace(" ", ""))
                    item, string = string.split(" ", 1)
                    rule.append(item.replace(" ", ""))
                    states[state].append(rule)
                
                if len(states[state]) != n:
                    del states[state]
            
            s = input()
        
        return states

    @classmethod
    def get_rules(cls):
        return deepcopy(cls.rules)
        
def main():
    pars = TuringParser()
    pars.add_rule("s0 | 3 ----> s0 | R | Em")
    print(pars.parse_table())

if __name__ == "__main__":
    main()