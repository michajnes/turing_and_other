from abc import ABC, abstractmethod
import sys
from typing import Callable, List, Tuple
from core.engine.cell import Cell
from core.utils.string_consts import Em, d_0, d_1, a,b,c,d,p,q, plus, ast, PP


class AbstractTape(ABC):
    @abstractmethod
    def _set_tape(self):
        pass

    @abstractmethod
    def move_right(self):
        pass
    
    @abstractmethod
    def get_curr_value(self):
        pass

    @abstractmethod
    def __len__(self):
        pass
    
    @abstractmethod
    def __str__(self):
        pass

    
class TuringTape(AbstractTape):
    
    __slots__ = ('l_tail', 'r_tail', 'head')

    def __init__(self, words: List[str], start_index: Tuple[int, int]):
        (self.l_tail, self.r_tail, self.head)  = self._set_tape(words, start_index)
    
    def external_func(self, func: Callable) -> 'TuringTape':
        func(self)
        return self
    
    def get_head(self) -> Cell:
        return self.head
    
    def change_head(self, new_symbol: str):
        self.head.set_symbol(new_symbol)
    
    def get_curr_value(self) -> str:
        return self.head.get_symbol()
    
    def get_left_tail(self) -> Cell:
        return self.l_tail
    
    def get_right_tail(self) -> Cell:
        return self.r_tail
    
    def still(self) -> None:
        return
    
    def move_left(self):
        if self.head.get_left() is None:
            self.elongate_to_left()
        self.head = self.head.get_left()

    def move_right(self):
        if self.head.get_right() is None:
            self.elongate_to_right()
        self.head = self.head.get_right()

    def add_cell_left(self):
        left_cell = Cell(Em, None, self.l_tail)
        self.l_tail.set_left(left_cell)
        self.l_tail = left_cell
    
    def add_cell_right(self):
        right_cell = Cell(E,  self.r_tail, None)
        self.r_tail.set_right(right_cell)
        self.r_tail = right_cell
    
    def __len__(self):
        curr_cell = self.l_tail
        index = 0
        while curr_cell:
            index += 1
            curr_cell = curr_cell.get_right()
        return index

    def elongate_to_left(self):
        l = len(self)
        for _ in range(l):
            self.add_cell_left()

    def elongate_to_right(self):
        l = len(self)
        for _ in range(l):
            self.add_cell_right()

    def visualize(self, state_name: str = ""):

        sys.stdout.write("\r")
        sys.stdout.flush()
        sys.stdout.write(f"{state_name}{' '*(6-len(state_name))}: {str(self)}")
        print()
    
    def __str__(self):
        
        curr_cell = self.get_left_tail()
        
        vis = "||"
        
        while curr_cell:
            if curr_cell == self.get_head():
                vis += "'"
            else:
                vis += " "
            sym = curr_cell.get_symbol()
            sym = sym if sym != Em else " "
            vis += sym + "|"
            curr_cell = curr_cell.get_right()
        
        return vis + "|"

    def _set_tape(self, words: List[str], start_index: Tuple[int, int]) -> Tuple[Cell, Cell, Cell]:
        def find_head(words: List[str], start_index: Tuple[int, int]) -> Cell:
            word, letter = start_index
            head = Cell(words[word][letter])
            return head
        
        def form_index(words, start_index: Tuple[int, int]) -> int:
            word, letter = start_index
            index = 0
            for i in range(word):
                index += 1 + len(words[i])
            return index + letter + 1
        
        def update_words(words: List[str], start_index: Tuple[int, int]) -> Tuple[str, str]:
            index = form_index(words, start_index)
            string = Em
        
            for word in words:
                string += word + Em
            
            left_word = string[:index]
            right_word = string[index+1:]
            return left_word, right_word
        
        
        head = l_tail = r_tail = find_head(words, start_index)
        l_word, r_word = update_words(words, start_index)

        for i in range(len(l_word)):
            left_cell = Cell(l_word[-i-1], None, l_tail)
            l_tail.set_left(left_cell)
            l_tail = left_cell
        for i in range(len(r_word)):
            right_cell = Cell(r_word[i], r_tail, None)
            r_tail.set_right(right_cell)
            r_tail = right_cell
        return l_tail, r_tail, head
