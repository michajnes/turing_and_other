import sys

sys.path.append('/class')


class Cell():
    __slot__ = ('__symbol','__left', '__right')

    def __init__(self, symbol='£', left=None, right=None):
        self.__symbol: str = symbol
        self.__left: Cell = left
        self.__right: Cell = right

    def set_left(self, left: 'Cell'):
        self.__left = left
    
    def get_left(self) -> 'Cell':
        return self.__left
    
    def set_right(self, right: 'Cell'):
        self.__right = right

    def get_right(self) -> 'Cell':
        return self.__right

    def set_symbol(self, symbol: str):
        self.__symbol = symbol
    
    def get_symbol(self) -> str:
        return self.__symbol


