class AlphabetError(Exception):
    def __init__(self, symbol, alphabet, message="This symbol is unexceptable!" + \
                                        "Please, write rules with symbols from the alphabet!"):
        self.symbol = symbol
        self.alphabet = alphabet
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"The symbol('s) {self.symbol} is(are) unexceptable!" +\
        f" Please, write rules with symbols from the alphabet: {self.alphabet}"

class MoveError(Exception):
    def __init__(self, move, message="Illegal value for moving!"):
        self.move = move
        self.message = message
        super().__init__(self.message)
    
    def __str__(self):
        return f"{self.move} is an illegal value for moving! It cannot be interpreted by Turing's Machine!"
