from abc import ABC, abstractmethod
from typing import List, NewType, Optional, Tuple

from rules.rules import tm_rules
from core.engine.exceptions import MoveError


TRule = NewType('TRule', Tuple[str, str, str, str, str])


class AbstractRule(ABC):
    @abstractmethod
    def get_symbols(self) -> List[str]:
        pass
    @abstractmethod
    def curr_state(self):
        pass
    @abstractmethod
    def next_state(self):
        pass
    @abstractmethod
    def build_obj(self, rule: TRule) -> 'AbstractRule':
        pass

class TuringRule(AbstractRule):
    __slots__ = ('rule', )
    def __init__(self, rule: Optional[TRule] = None):
        self.rule = rule

    def get_symbols(self) -> List[str]:
        if not self.rule:
            return
        return [self.rule[1], self.rule[4]]
    
    def curr_state (self):
        if not self.rule:
            return
        return self.rule[0]
    
    def next_state(self):
        if not self.rule:
            return
        return self.rule[2]
    
    def move(self):
        
        if not self.rule:
            return
        if self.rule[3] not in ['L', 'R', 'S']:
            raise MoveError(self.rule[3])
        
        return self.rule[3]
    
    def __str__(self):
        return f"|| {self.curr_state()} | {self.get_symbols()[0]} | ----> | {self.next_state()}" +\
            f" | {self.move()} | {self.get_symbols()[1]} ||"
    
    def build_obj(self, rule: Optional[TRule]) -> 'TuringRule':
        new_obj = TuringRule(rule)
        return new_obj


class FormRule():
    def forming(self, rules: List[TRule], empty_rule: AbstractRule) -> List[AbstractRule]:
        new_list = list()
        for rule in rules:
            new_list.append(empty_rule.build_obj(rule))
        return new_list


if __name__ == "__main__":
    for rule in tm_rules['copy']:
        r = TuringRule(rule)
        print(r)