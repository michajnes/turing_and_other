from __future__ import annotations
import os
import sys
import time
from typing import List, NewType, Tuple
from core.utils.string_consts import s0
from abc import ABC, abstractmethod
from core.engine.tape import AbstractTape, TuringTape
from core.engine.state_constructor import Interpreter, TuringCreator, TuringState


TRule = NewType('TRule', Tuple[str, str, str, str, str])


class Automaton(ABC):

    @abstractmethod
    def run(self, interpreter: Interpreter):
        pass
    
    @abstractmethod
    def results_extraction(self):
        pass

class TuringMachine(Automaton):

    __slots__ = ('final_alph', 'tape', 'states', 'interpretator', 'result', 'time', 'steps')

    def __init__(
            self, input_list: List[str] = list(), head_loc: tuple=tuple(), final_alphabet: List[str] = list(), 
            aid_alphabet: List[str] = list(), rules: List[TRule] = list(), initial_state: str = s0):
        self.final_alph = final_alphabet
        self.tape = TuringTape(input_list, head_loc)
        self.states = TuringCreator().build_graph(rules, final_alphabet+aid_alphabet)
        self.interpreter = Interpreter(self.states[initial_state], self.tape)
        self.result = ""
        self.time = 0
        self.steps = 0

    def run(self, verbose=False, sec_step=0, vis_step=1):
        result = False
        print("run")
        request_num = 0
        start = time.time()
        while not result:
            result = self.interpreter.request()
            if verbose and request_num % vis_step == 0:
                if sys.platform == 'win32':
                    os.system('cls')
                else:
                    os.system('clear')
                    
                state_name = self.interpreter.state.get_name()
                self.tape.visualize(state_name)
            request_num += 1
            time.sleep(sec_step)
            
        self.time = time.time()-start
        
        self.steps = request_num
        self.result = str(self.tape)
        
    
    def results_extraction(self):
        result = ""
        for ch in self.result:
            if ch in self.final_alph:
                result += ch
        self.result = result
        return self.result, self.time, self.steps

