from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Any, Dict, List, NewType, Tuple, Union
from core.engine.exceptions import AlphabetError
from core.engine.rule import AbstractRule, FormRule, TuringRule
from core.engine.tape import AbstractTape
from rules.rules import default_rule, tm_rules
from core.utils.string_consts import s0, sf, d_1, d_0, ast, PP, plus, a, b, c, d, p, q, Em


TRule = NewType('TRule', Tuple[str, str, str, str, str])


class Interpreter():
    
    __slots__ = ('state',)
    _state = None

    def __init__(self, state: State, tape: AbstractTape):
        self.transition_to(state, tape)
    
    def transition_to(self, state: State, tape: AbstractTape):
        self.state = state
        self.state.context = self
        self.state.tape = tape
    
    def request(self):
        state = self.state.handle()
        if state == sf:
            return True
        return False


class State(ABC):
    
    @property
    def context(self) -> Interpreter:
        return self._context

    @context.setter
    def context(self, context: Interpreter):
        self._context = context
    
    @property
    def tape(self) -> AbstractTape:
        return self._tape

    @tape.setter
    def tape(self, tape: AbstractTape):
        self._tape = tape

    @abstractmethod
    def handle(self):
        pass


class TuringState(State):

    __slots__ = ('states', 'actions', 'name')

    def __init__(self):
        
        self.states = dict()
        self.actions = dict()
        self.name = str()
    
    def set_states(self, states: Dict[str, State]) -> None:
        self.states = states
    
    def set_name(self, name: str) -> None:
        self.name = name

    def get_name(self) -> str:
        return self.name

    def set_action(self, key: str, value: Any):
        self.actions[key] = value

    def handle(self) -> str:
        key = self.tape.get_curr_value()

        self.context.transition_to(self.actions[key][0], self.tape)
        
        self.actions[key][2](self.tape)
        self.actions[key][1](self.tape)

        return self.actions[key][0].get_name()


class Creator(ABC):
    @abstractmethod
    def build_graph(self, rules: List[TRule], alphabet: List[str]):
        pass

    @abstractmethod
    def check_rules(self, rules : List[TRule], alphabet: List[str]):
        pass


class TuringCreator(Creator):

    __slots__ = tuple()

    def build_graph(self, rules: List[TRule], alphabet : List[str], initial_state=s0):
        rules = FormRule().forming(rules, TuringRule())

        check_result, symbols = self.check_rules(rules, alphabet)

        states = dict()
        state_names = set()

        if check_result:
            # creating a state names list
            for rule in rules:
                state_names.add(rule.curr_state())
                state_names.add(rule.next_state())

            # creating the State instance for each state name from rules 
            for name in state_names:
                states[name] = TuringState() 
                states[name].set_name(name)
                
            for name in state_names:
                states[name].set_states(states)
            move_func = {
                'L' : lambda x: x.move_left(),
                'R' : lambda x: x.move_right(),
                'S' : lambda x: x.still()
            }

            for rule in rules:
                symbol_func = (lambda z: lambda y: y.change_head(z))(rule.get_symbols()[1])
                triple = (states[rule.next_state()], move_func[rule.move()], symbol_func)
                states[rule.curr_state()].set_action(rule.get_symbols()[0], triple) 
        else:
            raise AlphabetError(symbols, alphabet)

        return states

    def check_rules(self, rules : List[TRule], alphabet : List[str]):
        for rule in rules:
            symbols = rule.get_symbols()
            if sum([s in alphabet for s in symbols]) < len(symbols):
                return False, symbols
        return True, list()


if __name__ == "__main__":
    alphabet = [d_1, d_0, Em, ast, PP, plus, a,b,c,d,p,q]
    states = TuringCreator().build_graph(tm_rules.get('x+y', default_rule(alphabet)), alphabet)
    