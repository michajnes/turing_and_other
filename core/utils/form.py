with open("util.py", 'w') as f:
    f.write("E = \"£\"\n")
    f.write("PP = \"^\"\n")
    f.write("plus = \"+\"\n")
    f.write("ast = \"*\"\n")
    l = []
    for letter in 'abcdefghjklmnpqrstux':
        for ind1 in range(8): 
            f.write(letter + str(ind1) + " = " + "\'" + letter+ str(ind1) + "\'\n")
            for let in 'feradh':
                f.write(letter + str(ind1) + str(let) + " = " + "\'" + letter + str(ind1) + str(let) + "\'\n")
            for ind2 in range(5):
                f.write(letter + str(ind1) + str(ind2) + " = " + "\'" + letter + str(ind1) + str(ind2) + "\'\n")

        for let in 'feradh':
            f.write(letter + str(let) + " = " + "\'" + letter+ str(let) + "\'\n")
            for ind1 in range(7):
                f.write(letter + let + str(ind1) + " = " + "\'" + letter + let + str(ind1) +  "\'\n")

                for ind2 in range(5):
                    f.write(letter + let + str(ind1) + str(ind2) + " = " + "\'" + letter + let + str(ind1) + str(ind2) + "\'\n")

    for letter in 'abcdefghijklmnopqrstuvwxyzABCDFGHIJKLMNOPQRSTUVWXYZ':
        f.write(letter + " = " + "\'" + letter + "\'\n")
    for dig in range(10):
        f.write("d_" + str(dig) + " = " + "\'" + str(dig) + "\'\n")
    f.write("cyr = {}\n")
    for letter in 'абвгґдеєжзиіїйклмнопрстуфхцчшщьюяэёъыАБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЬЮЯЭЁЪЫ':
        f.write("cyr[\'" + str(ord(letter)) + "\'] = " + "\'" + letter + "\'\n")