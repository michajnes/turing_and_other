import json
from rules import tm_rules

y = json.dumps(tm_rules, indent=2, sort_keys=True)
with open('rules_json.json', 'w') as f:
    f.write(y)