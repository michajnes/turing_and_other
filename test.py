import time
import os

from core.engine.automaton import TuringMachine
from rules.rules import tm_rules
from core.engine.tape import AbstractTape, TuringTape
from core.engine.cell import Cell
from utils.string_consts import d_0, d_1, Em, ast, PP, plus, a, b, c, d, p, q

os.system('clear')

def test_visual(tape : AbstractTape):
    
    i = 0
    while i < 5:
        while True:
            if tape.get_head().get_left() is None:
                break
            tape.move_left()
            os.system('clear')
            tape.visualize()
            time.sleep(0.02)
        
        while True:
            if tape.get_head().get_right() is None:
                break
            tape.move_right()
            os.system('clear')
            tape.visualize()
            time.sleep(0.02)
        i += 1
        
    

def test_bonds(cell : Cell, direction):
    func = None
    if direction == 'left':
        func = lambda cell: cell.get_left()
    elif direction == 'right':
        func = lambda cell: cell.get_right()
    else:
        return
    string = "'"
    curr_cell = cell
    print(curr_cell.get_symbol())
    start = time.time()
    while curr_cell:
        sym = curr_cell.get_symbol()
        string += sym
        curr_cell = func(curr_cell)
        

    print('\n',time.time() - start, "secs")
    print(string+"'")

def test(test_list):
    for i in range(0, 3):
        for j in range(0, 3):
            w_num = i % len(test_list)
            l_num = j % len(test_list[w_num])
            new_tape = TuringTape(test_list, (w_num, l_num))
            head = new_tape.get_head()
            l_tail = new_tape.get_left_tail()
            r_tail = new_tape.get_right_tail()
            test_bonds(head, 'left')
            test_bonds(head, 'right')
            test_bonds(l_tail, 'right')
            test_bonds(r_tail, 'left')


def main():
    test_list_2 = ["11^11"]
    
    tm = TuringMachine(test_list_2, (0,0), [d_0,d_1, ast, PP, plus], [Em ,a,b,c,d, p, q], rules=tm_rules['x^yb'])

    tm.run(verbose=True, sec_step=0.1)
    result, time_in_sec, steps = tm.results_extraction()
    result = int('0b'+result, base=2)
    print(f"Result: {result}")
    print(f"Executed for {steps} steps")
    print(f"Time: {time_in_sec} secs")

if __name__ == "__main__":

    main()