from typing import Callable, List, Tuple
from core.utils.string_consts import Em, d_0, d_1, a,b,c,d,p,q, plus, ast, PP
from core.engine.automaton import TuringMachine
import argparse
from rules.rules import tm_rules


def pars_args() -> argparse.Namespace:
    """
	Terminal arguments' parser:
		--verbose - the algorithm process is shown if it present 
		--vis_step (int) - if it equals to n each nth step is shown 
							default value: 1
		--sec_step - defines in seconds the pause between each shown step
							default value: 0
	"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', help='Print more data', default=False,
    action='store_true')
    parser.add_argument('--vis_step', type=int, default=1)
    parser.add_argument('--sec_step', type=float, default=0)
    args = parser.parse_args()
    return args


def run_func(
        func_name: str, final_alph: List[str], aid_alph: List[str], 
        input_list: List[str], head_coords: Tuple[int, int]) -> None:
    """
	Runs the choosen algorithm and shows results: 
		func_name
	
	"""
    args = pars_args()
    tm = TuringMachine(input_list, head_coords, final_alph, 
                        aid_alph, rules=tm_rules[func_name])
    tm.run(verbose=args.verbose, sec_step=args.sec_step, 
            vis_step=args.vis_step)
    result, time_in_sec, steps = tm.results_extraction()

    if func_name != "copy":
    	
        result = int('0b'+result, base=2)
		
    print(f"Result: {result}")
    print(f"Executed for {steps} steps")
    print(f"Time: {time_in_sec} secs")


def summ_bin(x: int, y: int) -> None: 
    run_func("x+yb", [d_1, d_0, ast, PP, plus], [Em, a,b,c,d,p,q], 
            [bin(x)[2:]+'+'+bin(y)[2:]], (0,0))


def prod_bin(x: int, y: int) -> None: 
    run_func("x*yb", [d_1, d_0, ast, PP, plus], 
			[Em, a,b,c,d,p,q], [bin(x)[2:]+'*'+bin(y)[2:]], (0,0))


def power_bin(x: int, y: int) -> None: 
	run_func("x^yb", [d_1, d_0, ast, PP, plus], 
			[Em, a,b,c,d,p,q], [bin(x)[2:]+'^'+bin(y)[2:]], (0,0))


def copy(x: int) -> None:
	run_func("copy", [d_1, d_0, ast, PP, plus], 
			[Em, a,b,c,d,p,q], [bin(x)[2:]], (0,0))


def input_data() -> Tuple[int, int, Callable]:
    x = int(input("x: "))
    y = int(input("y: "))
    f = input("func: ")

    if f == '+':	
        func = summ_bin
	
    elif f == '*':
        func = prod_bin
	
    elif f == '^':
        func = power_bin
	
    elif f == 'c':
        func = copy
        return x, None, func
	
    return x, y, func


def main(): 
	
	x, y, func = input_data()
	if y is not None:
		func(x, y)
	else:
		func(x)

if __name__ == "__main__":
	main()
